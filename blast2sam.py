#!/usr/bin/env python

import sys, re, os, argparse
import xml.etree.cElementTree as ET
from string import maketrans

def construct_cigar(query_seq, subj_seq, query_len, query_from, query_to):
    cigar_string = list()
    last_op  = None
    last_op_length =0
    if(query_from > query_to):
        print >>sys.stderr, "Found a reversed query...edge case unhandled"
        sys.exit(1)
    if (query_from > 1):
        cigar_string.append( str((query_from -1)) + "H")
    for i in range(0, len(query_seq)):
        if(query_seq[i] == '-'):
            #query sequence has a gap which means a deletion compared to reference
            current_op='D'
        elif(subj_seq[i]=='-'):
            # subject sequence has a gap which means insertion compared to reference
            current_op='I'
        else:
            # "match" -- may still be a snp, but no gap alignment needed
            current_op='M'
        if(current_op != last_op):
            if(last_op):
                cigar_string.append(str(last_op_length) + last_op)
            last_op = current_op
            last_op_length = 1
        else:
            last_op_length+=1
    cigar_string.append(str(last_op_length) + last_op)
    if(query_to < query_len):
        cigar_string.append(str((query_len - query_to)) + "H")
    return cigar_string



def parse_doc_string(string):
    root = ET.fromstring(string)
    iterations = root.find("BlastOutput_iterations")
    mapq_sam5=255 #hardcode no mapping quality is associated in blast
    rnext_sam7='*'
    pnext_sam8='0'
    tlen_sam9='0'
    qual_sam11='*'
    for iteration in iterations.findall("Iteration"):
        query_def = iteration.find("Iteration_query-def").text
        if(re.search(" ", query_def)):
            (qname_sam1, _) = query_def.split(" ", 1) #knock off any trailing shit
        else:
            qname_sam1 = query_def

        query_len = iteration.find("Iteration_query-len").text
        for hit in iteration.find("Iteration_hits").findall("Hit"):
            rname_sam3 = hit.find("Hit_id").text
            hit_hsp = hit.find("Hit_hsps").find("Hsp")
            query_sequence = hit_hsp.find("Hsp_qseq").text
            subject_sequence = hit_hsp.find("Hsp_hseq").text
            seq_sam10=query_sequence.replace("-", "") #FIXME NEED TO REVERSE?
            subject_from = hit_hsp.find("Hsp_hit-from").text #FIXME reverse for nucleotide shit sometimes?
            subject_to = hit_hsp.find("Hsp_hit-to").text
            query_from = hit_hsp.find("Hsp_query-from").text
            query_to = hit_hsp.find("Hsp_query-to").text
            cigar_list = construct_cigar(query_sequence, subject_sequence, int(query_len), int(query_from), int(query_to))
            flag_sam2=0
            pos_sam4 = subject_from
            if(int(subject_to) < int(subject_from)): #we aligned to this area backwards. 
                print >>sys.stderr, "reversing read/ref: %s, %s" % (qname_sam1, rname_sam3)
                #fill out flag_sam2, reverse cigar
                cigar_list.reverse()
                flag_sam2 |= 16 #0x10, reverse complemented sequence to comply with sam             
                seq_sam10 = seq_sam10.translate(maketrans("atgcrymkswATGCRYMKSW","tacgyrkmswTACGYRKMSW"))
                seq_sam10= seq_sam10[::-1]
                pos_sam4 = subject_to
            cigar_sam6 = ''.join(cigar_list)
            print "\t".join(map(str, [qname_sam1, flag_sam2, rname_sam3, pos_sam4, mapq_sam5,cigar_sam6,rnext_sam7,pnext_sam8,tlen_sam9,seq_sam10,qual_sam11]))







xml_file = sys.argv[1]
xml_fh = open(xml_file, "r")
xml_doc_string= xml_fh.readline().rstrip()
header_line = xml_doc_string
while(1):
    line = xml_fh.readline()
    if not line:
        break
    if(re.search("</BlastOutput>", line)):
        xml_doc_string += "</BlastOutput>"
        tmp_fh = open("/tmp/tmp_xml_doc", "w")
        tmp_fh.write(xml_doc_string)
        parse_doc_string(xml_doc_string)
        xml_doc_string = header_line
    else:
        xml_doc_string = xml_doc_string + line




